angular.module('myApp').config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            template: '<example-component></example-component>'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);