angular.module('exampleComponent').directive('exampleComponent', function() {
    return {
        restrict: 'E',
        templateUrl: 'app/components/example-component/example-component.html',
        controller: 'ExampleComponentController'
    };
});