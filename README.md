# My AngularJS App

This is a basic AngularJS application.

## Getting Started

1. Clone the repository.
2. Install the dependencies using `npm install`.
3. Open `index.html` in your browser to see the application.
